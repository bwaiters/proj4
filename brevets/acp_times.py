"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

def distance_open(distance):
    total_run =0
    #anything greater than 200 we can calculate for based off of
    #https://rusa.org/pages/acp-brevet-control-times-calculator
    #dividing by maximum speed in km/hr
    while distance > 200:
        if distance > 1000:
            total_run += (distance - 1000)/26
            distance -= distance - 1000
        elif distance > 600:
            total_run += (distance - 600)/28
            distance -= distance - 600
        elif distance > 400:
            total_run += (distance - 400)/30
            distance -= distance - 400
        else:
            total_run += (distance - 200)/32
            distance -= distance - 200
    total_run += distance/34
    return total_run

def distance_close(distance):
        total_run =0
        #anything greater than 200 we can calculate for based off of
        #https://rusa.org/pages/acp-brevet-control-times-calculator
        #dividing by minimum speed in km/hr
        while distance > 200:
            if distance > 1000:
                total_run += (distance - 1000)/13.333
                distance -= distance - 1000
            elif distance > 600:
                total_run += (distance - 600)/11.428
                distance -= distance - 600
            elif distance > 400:
                total_run += (distance - 400)/15
                distance -= distance - 400
            else:
                total_run += (distance - 200)/15
                distance -= distance - 200
        total_run += distance/15
        return total_run



def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    open_time = distance_open(control_dist_km)
    start_time = arrow.get(brevet_start_time)
    print("start time: ", brevet_start_time)
    hour = math.floor(open_time
    )
    mins = round((open_time - hour) * 60)
    shifting = start_time.shift(minutes=mins,hours=hour)
    #https://pythontic.com/datetime/datetime/isoformat
    return shifting.isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    open_time = arrow.get(brevet_start_time)
    if control_dist_km == 0.0:
        return open_time.shift(hours=+1).isoformat()
    elif control_distance_km == 200.0 and brevet_dist_km == 200:
        return open_time.shift(hours=+13,minutes=+30).isoformat()
    elif control_distance_km == 300.0 and brevet_dist_km == 300:
        return open_time.shift(hours=+20).isoformat()
    elif control_distance_km == 400.0 and brevet_dist_km == 400:
        return open_time.shift(hours=+27).isoformat()
    elif control_distance_km == 600.0 and brevet_dist_km == 600:
        return open_time.shift(hours=+40).isoformat()
    elif control_distance_km == 1000.0 and brevet_dist_km == 1000:
        return open_time.shift(hours=+75).isoformat()
    else:
        close_time = distance_close(control_distance_km)
        open_time = arrow.get(brevet_start_time)
        print("start time: ", brevet_start_time)
        hour = math.floor(close_time)
        mins = round((close_time - hour) * 60)
        shifting = open_time.shift(minutes=mins,hours=hour)
        return shifting.isoformat()
